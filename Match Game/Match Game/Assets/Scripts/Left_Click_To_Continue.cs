﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Left_Click_To_Continue : MonoBehaviour {
    public bool should_close_game;

    public string level_to_load;
    public Image black_background;

    private bool doitonce = false;
    // Use this for initialization
    void Start () {
        AudioListener.volume = 0.5f;
        black_background.gameObject.SetActive(true);
        black_background.canvasRenderer.SetAlpha(1.0f);
        black_background.CrossFadeAlpha(0.0f, 2.0f, false);

    }
	

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1"))
        {
            if (should_close_game == false)
                if (doitonce == false)
                {
                    doitonce = true;
                    StartCoroutine(Load_Next_Level());
                }


            else if (should_close_game == true)
                if (doitonce == false)
                {
                    doitonce = true;
                    StartCoroutine(Close_Game());
                }
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
            Close_Game();
        }

    }

    IEnumerator Load_Next_Level()
    {
        black_background.canvasRenderer.SetAlpha(0.0f);
        black_background.CrossFadeAlpha(1.0f, 2.0f, false);
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene(level_to_load);        
    }

    IEnumerator Close_Game()
    {
        black_background.canvasRenderer.SetAlpha(0.0f);
        black_background.CrossFadeAlpha(1.0f, 2.0f, false);
        yield return new WaitForSeconds(2.0f);
        Application.Quit();
    }

}
