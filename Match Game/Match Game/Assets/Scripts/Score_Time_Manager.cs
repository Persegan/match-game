﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Score_Time_Manager : MonoBehaviour {

    public static Score_Time_Manager ScoreTimeManager;

    public float time;
    public float score;

    void Awake()
    {
        if (ScoreTimeManager == null)
        {
            DontDestroyOnLoad(gameObject);
            ScoreTimeManager = this;
            SceneManager.sceneLoaded += OnSceneWasLoaded;
        }
        else if (ScoreTimeManager != this)
        {
            Destroy(gameObject);
        }
    }

	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
	}


    void OnSceneWasLoaded(Scene scene, LoadSceneMode mode)
    {

        switch (scene.name)
        {
            case ("Gameplay_Scene"):
                Reset();
                break;   


            default:

                break;
        }
    }

    public void Reset()
    {
        ScoreTimeManager.time = 0.0f;
        ScoreTimeManager.score = 0.0f;
    }
}
