﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawner : MonoBehaviour {

    public static int enemy_number;

    public GameObject Enemy;

    public float spawntime;

    private float currenttime;

	// Use this for initialization
	void Start () {
        currenttime = spawntime;
        enemy_number = 0;
	}
	
	// Update is called once per frame
	void Update () {
        currenttime -= Time.deltaTime;

        if (currenttime <= 0)
        {
            if (enemy_number > 5)
            {
                currenttime = spawntime;
            }
            else
            {
                enemy_number++;
                Vector3 auxiliar = transform.GetChild(3).transform.position;
                auxiliar.x += 2.0f;
                GameObject auxiliar_gameobject = GameObject.Instantiate(Enemy, auxiliar, Quaternion.identity);
                auxiliar_gameobject.GetComponent<NavMeshAgent>().speed += Score_Time_Manager.ScoreTimeManager.time / 10;
                spawntime -= 0.1f;
            }          
        }

	}
}
