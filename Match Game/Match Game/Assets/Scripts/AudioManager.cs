﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour {

    public static AudioManager Audio_Manager;

    public AudioClip Main_Theme;

    void Awake()
    {
        if (Audio_Manager == null)
        {
            DontDestroyOnLoad(gameObject);
            Audio_Manager = this;
            SceneManager.sceneLoaded += OnSceneWasLoaded;
        }
        else if (Audio_Manager != this)
        {
            Destroy(gameObject);
        }
    }

    void OnSceneWasLoaded(Scene scene, LoadSceneMode mode)
    {

        switch (scene.name)
        {
            case ("Gameplay_Scene"):

                if (GetComponent<AudioSource>().clip == Main_Theme && GetComponent<AudioSource>().isPlaying && GetComponent<AudioSource>().volume == 1.0f)
                {

                }
                else
                {
                    StopAllCoroutines();
                    GetComponent<AudioSource>().volume = 0.0f;
                    GetComponent<AudioSource>().clip = Main_Theme;
                    GetComponent<AudioSource>().Play();
                    StartCoroutine(FadeIn(GetComponent<AudioSource>(), 5.0f));
                }
                break;
            case ("GameOver_Scene"):
                {
                    StopAllCoroutines();
                    StartCoroutine(FadeOut(GetComponent<AudioSource>(), 5.0f));
                }

                break;


            default:

                break;
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
    }


    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.Stop();
        //audioSource.volume = startVolume;
    }

    public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;
        audioSource.Play();
        while (audioSource.volume < 1)
        {
            audioSource.volume += 1 * Time.deltaTime / FadeTime;

            yield return null;
        }

        //audioSource.Stop();
        //audioSource.volume = startVolume;
    }
}
