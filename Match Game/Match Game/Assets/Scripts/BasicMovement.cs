﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class BasicMovement : MonoBehaviour {

    public float speed = 10.0f;
    public float rotationSpeed = 2.0f;

    public float maxHeadRotation = 80.0f;
    public float minHeadRotation = -160.0f;

    public float jumpSpeed = 15.0f;
    public float gravity = 30.0f;

    public Transform head;

    private float currentHeadRotation = 0;
    private float yVelocity = 0;
    private Vector3 moveVelocity = Vector3.zero;

    public GameObject Explosion;

    public float powerup_duration;


    private float powerup_timer = 0.0f;

    public AudioClip Dying_Sound;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        Vector2 mouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        transform.Rotate(Vector3.up, mouseInput.x * rotationSpeed);

        currentHeadRotation = Mathf.Clamp(currentHeadRotation + mouseInput.y * rotationSpeed, minHeadRotation, maxHeadRotation);

        head.localRotation = Quaternion.identity;
        head.Rotate(Vector3.left, currentHeadRotation);

       

        yVelocity -= gravity * Time.deltaTime;

        if (GetComponent<CharacterController>().isGrounded)
        {
            yVelocity = 0;
            if (Input.GetButton("Jump"))
            {
                yVelocity = jumpSpeed;
            }

            //moveVelocity = transform.TransformDirection(new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"))) *speed;

        }
        //Vector3 velocity = moveVelocity + yVelocity * Vector3.up;

        gameObject.GetComponent<CharacterController>().Move(transform.TransformDirection(input * speed * Time.deltaTime + yVelocity * Vector3.up * Time.deltaTime));
        //gameObject.GetComponent<CharacterController>().Move(velocity * Time.deltaTime);

        if (powerup_timer > 0)
        {
            powerup_timer -= Time.deltaTime;
            if (powerup_timer <= 0)
            {
                transform.GetChild(1).gameObject.SetActive(true);
                transform.GetChild(2).gameObject.SetActive(false);
                GetComponent<Projectile_Launcher>().reloadTime = 0.75f;
            }
        }

    }


    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            GameOver();
        }
        if (col.gameObject.tag == "PowerUp")
        {
            Destroy(col.gameObject);
            powerup_timer = powerup_duration;
            transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(2).gameObject.SetActive(true);
            GetComponent<Projectile_Launcher>().reloadTime = 0.1f;

        }
    }

    public void GameOver()
    {
        StartCoroutine(GameOver_coroutine());
    }

    IEnumerator GameOver_coroutine()
    {
        GameObject auxiliarobject = GameObject.FindGameObjectWithTag("EffectsManager");
        auxiliarobject.transform.position = gameObject.transform.position;
        auxiliarobject.GetComponent<AudioSource>().PlayOneShot(Dying_Sound);
        GameObject.Instantiate(Explosion, gameObject.transform.position, gameObject.transform.rotation);
        GetComponent<BasicMovement>().enabled = false;
        GetComponent<Projectile_Launcher>().enabled = false;
        GetComponent<CharacterController>().enabled = false;
        transform.GetChild(1).gameObject.SetActive(false);
        transform.GetChild(2).gameObject.SetActive(false);
        yield return new WaitForSeconds(3.0f);
        SceneManager.LoadScene("GameOver_Scene");

    }
}
