﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile_Launcher : MonoBehaviour {

    public GameObject projectilePrefab;
    public Transform launcherBarrel;
    public float reloadTime = 0.5f;
    public AudioClip Shoot_Sound;

    private float lastFireTime;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButton("Fire1") && Time.time > lastFireTime + reloadTime)
        {
            GetComponent<AudioSource>().PlayOneShot(Shoot_Sound);
            Vector3 auxiliar = launcherBarrel.transform.forward;
            auxiliar.y += 0.085f;
            GameObject go = (GameObject)Instantiate(projectilePrefab, launcherBarrel.position, Quaternion.LookRotation(auxiliar));
            Physics.IgnoreCollision(GetComponent<Collider>(), go.GetComponent<Collider>());
            lastFireTime = Time.time;
        }
    }
}
