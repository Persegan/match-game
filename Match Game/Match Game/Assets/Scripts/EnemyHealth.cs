﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    public float initialHealth = 10.0f;

    private float currentHealth;

    public GameObject Explosion;
    public GameObject Powerup;

    public AudioClip Enemy_dies_sound;

    void Start()
    {
        currentHealth = initialHealth;
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;

        if (currentHealth < 0)
        {
            Kill();
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Projectile")
        {
            TakeDamage(col.gameObject.GetComponent<Projectile>().damageDealt);
        }
    }

    public void Kill()
    {
        GameObject auxiliarobject = GameObject.FindGameObjectWithTag("EffectsManager");
        auxiliarobject.transform.position = gameObject.transform.position;
        auxiliarobject.GetComponent<AudioSource>().PlayOneShot(Enemy_dies_sound);
        GameObject.Instantiate(Explosion, gameObject.transform.position, gameObject.transform.rotation);
        int auxiliar = Random.Range(0, 6);
        if (auxiliar == 5)
        {
            GameObject.Instantiate(Powerup, gameObject.transform.position, gameObject.transform.rotation);
        }
        Score_Time_Manager.ScoreTimeManager.score += 100;
        EnemySpawner.enemy_number--;
        Destroy(gameObject);
    }
}
