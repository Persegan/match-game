﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver_Text : MonoBehaviour {

    public Text game_over_text;

	// Use this for initialization
	void Start () {
        game_over_text.text = "You score was: \n" + Score_Time_Manager.ScoreTimeManager.score + "\n You survived for: \n" + Score_Time_Manager.ScoreTimeManager.time + " seconds \n Press Left Click to play again \n or \n Press ESC to exit" ;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
