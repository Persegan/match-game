﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float speed = 20.0f;
    public float life = 5.0f;
    public float damageDealt = 1.0f;

    public GameObject Explosion;
    public AudioClip shot_impacts_sound;

	// Use this for initialization
	void Start () {
        Invoke("Kill", life);		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += transform.forward * speed * Time.deltaTime;
	}


    void OnTriggerEnter(Collider col)
    {
        Kill();
    }

    void Kill()
    {
        GameObject auxiliarobject = GameObject.FindGameObjectWithTag("EffectsManager");
        auxiliarobject.transform.position = gameObject.transform.position;
        auxiliarobject.GetComponent<AudioSource>().PlayOneShot(shot_impacts_sound);
        GameObject.Instantiate(Explosion, gameObject.transform.position, gameObject.transform.rotation);
        Destroy(gameObject);
    }
}
